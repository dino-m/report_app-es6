#Street and Park report app

JS application created with ES6 features, for making report for streets and parks.

###DESCRIPTION:

- App allows to input some streets and parks (one at the time).
- After each input app will calculate total and average length of streets, average park age and tree density of each park.
- User can also delete items (parks or streets) and then app will recalculate all data again.