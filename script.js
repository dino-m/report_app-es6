let reportController = (() =>{

	class Element{
		constructor(name, buildYear){
			this.name  = name;
			this.buildYear = buildYear;
		}
	}

	class Park extends Element{
		constructor(name, numTrees, area, buildYear){
			super(name, buildYear);
			this.area = area; // km2
			this.numTrees = numTrees;
		}

		treeDensity(){
			const density = this.numTrees / this.area;
			return density.toFixed(2);
		}
	}

	class Street extends Element{
		constructor(name, length, size, buildYear){
			super(name, buildYear);
			this.length = length;
			this.size = size;
		}
	}

	let calcDataStreet = new Map();
	let calcDataPark = new Map();

    return {
    	randomNumb: () => {
			let randomId;
            randomId = Math.floor(Math.random() * 100) + 1;
            return randomId;
    	},

	    addItem: (id, type, name, lengthTreeNumb, size, buildYear) => {
	        let newItem, newId;
	        let returnArr = [];

	        // Create new item based on name of park or street
	        if (type === 's') {
	            newItem = new Street(name, lengthTreeNumb, size, buildYear);

	           	checkIdStreet = Array.from(calcDataStreet.keys());

	           	if(checkIdStreet.length > 0){
		        	if (checkIdStreet.includes(id)){
		        		id = randomNumb();
		        		calcDataStreet.set(id, newItem.length);
		        	}else{
		        		calcDataStreet.set(id, newItem.length);
		        	}
	           	}else{
	           		calcDataStreet.set(id, newItem.length);
	           	}
	            
	        }else if (type === 'p') {
	            newItem = new Park(name, lengthTreeNumb, size, buildYear);

	           	checkIdPark = Array.from(calcDataPark.keys());

	           	if(checkIdPark.length > 0){
		        	if (checkIdPark.includes(id)){
		        		id = randomNumb();
		        		calcDataPark.set(id, newItem.buildYear);
		        	}else{
		        		calcDataPark.set(id, newItem.buildYear);
		        	}
	           	}else{
	           		calcDataPark.set(id, newItem.buildYear);
	           	}
	        }

	        newId = id;

	        returnArr = [newItem, newId];
	        
	        // Return the new element
	        return returnArr;
	    },

	    deleteItemStructure: (id, type) => {

	    	if(type === 's'){
	    		calcDataStreet.delete(id);
	    	}else if(type === 'p'){
	    		calcDataPark.delete(id);
	    	}
	    },

	   	calcGlobal: () => {
	    	let yearArr, avgPark = 0, avgStreet = 0, sumLength = 0, countStreet = 0, sumPark = 0, countPark = 0;
	    	let calcArr = [];

	    	calcArrStreet = Array.from(calcDataStreet.values());
	    	calcArrPark = Array.from(calcDataPark.values());

	    	// Calculation of total streets length
	    	if(calcArrStreet.length > 0){
	    		for(const cur of calcArrStreet){
		    		sumLength += cur;
		    		countStreet ++;
	    		}
				
				// Calculation of average street length
	    		if(sumLength > 0){
	    			avgStreet = sumLength / countStreet;
	    		}else{
    				avgStreet = 0;
    			}
	    	}

	    	// Calculation of average park years
	    	if(calcArrPark.length > 0){
		    	yearArr = calcArrPark.map(cur => new Date().getFullYear() - cur);

		    	for(const cur of yearArr){
		    		sumPark += cur;
		    		countPark ++;
		    	}

			    if(sumPark > 0){
			    	avgPark = sumPark / countPark;
			    }else{
			    	avgPark = 0;
			    }
	    	}

	    	calcArr = [avgPark.toFixed(2), sumLength.toFixed(2), avgStreet.toFixed(2)];

	    	return calcArr;
		}
	}
})();

let UIController = (() =>{

	const DOMstrings = {
		addNameCity: '.add_city',
		addNameStreet: '.add_street_name',
		addLengthStreet: '.add_street_length',
		addSizeStreet: '.add_street_size',
		addNamePark: '.add_park_name',
		addTreeNumbPark: '.add_park_trees',
		addSizePark: '.add_park_size',
		addBuildYearPark: '.add_park_year',
		addBuildYearStreet: '.add_street_year',
		saveStreetBtn: '.save_street_btn',
		saveParkBtn: '.save_park_btn',
		reportBtn: '.report_btn',
		globalList: '.report_global',
		streetList: '.report_street_list',
		parkList: '.report_park_list',
		dateLabel: '.onDay',
		streetInputs: '.street_input_box input',
		parkInputs: '.park_input_box input',
		deleteBox: '.report_street_park_box',
		deleteBtn: '.del_btn',
		labelAvgPark: '.avg_years_park',
		labelTotalStreets: '.total_streets',
		labelAvgStreets: '.avg_streets',
		labelParkList: '.global_park_list'
    }

    return {
	    getInput: () => {
	        return {
	            nameStreet: document.querySelector(DOMstrings.addNameStreet).value,
	            lengthStreet: parseFloat(document.querySelector(DOMstrings.addLengthStreet).value),
	            buildYearStreet: parseInt(document.querySelector(DOMstrings.addBuildYearStreet).value),
	            sizeStreet: document.querySelector(DOMstrings.addSizeStreet).value, // returns tiny, small, normal, big or huge
	            namePark: document.querySelector(DOMstrings.addNamePark).value,
	            treeNumbPark: parseInt(document.querySelector(DOMstrings.addTreeNumbPark).value),
	            sizePark: parseFloat(document.querySelector(DOMstrings.addSizePark).value),
	            buildYearPark: parseInt(document.querySelector(DOMstrings.addBuildYearPark).value)
	        };
	    },

	    getDOMstrings: () => {
            return DOMstrings;
        },

        addListItem: (obj, type, id) => {
            let html, element;

            document.querySelector(DOMstrings.globalList).style.display = 'block';
            
            // Create HTML strings
            if (type === 's') {
                element = DOMstrings.streetList;
                
                html = `<div id="s-${id}" class="item_street"><div class="del_btn">X</div><h3>${obj.name}</h3><p>Length: ${obj.length} km</p><p>Build year: ${obj.buildYear}</p><p>Size classification: ${obj.size}</p><hr></div>`;

            } else if (type === 'p') {
                element = DOMstrings.parkList;
                
                html = `<div id="p-${id}" class="item_park"><div class="del_btn">X</div><h3>${obj.name}</h3><p>Tree density: ${obj.treeDensity()} per square km.</p><p>Area size: ${obj.area} km2</p><p>Build year: ${obj.buildYear}</p><hr></div>`;
            }

            // Insert the HTML into the DOM
            document.querySelector(element).insertAdjacentHTML('beforeend', html);
        },

        clearFields: () => {
            var fields, fieldsArr;
            
            fields = document.querySelectorAll(DOMstrings.addNameStreet + ', ' + 
            								  DOMstrings.addLengthStreet + ', ' + 
            								  DOMstrings.addBuildYearStreet + ', ' + 
            								  DOMstrings.addNamePark + ', ' + 
            								  DOMstrings.addTreeNumbPark + ', ' + 
            								  DOMstrings.addSizePark + ', ' + 
            								  DOMstrings.addBuildYearPark);
            
            fieldsArr = Array.from(fields);
            
            for(const cur of fieldsArr) {
                cur.value = "";
                cur.disabled = false;
            }

            // disable also street size (select input)
            document.querySelector(DOMstrings.addSizeStreet).disabled = false;

            //document.querySelector(DOMstrings.addNameCity).value = 'os';
            document.querySelector(DOMstrings.addSizeStreet).value = 'normal';   
        },

        displaySubtitle: () => {
            let now, month, year;
            const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            
            now = new Date();

            month = now.getMonth();
            
            year = now.getFullYear();
            document.querySelector(DOMstrings.dateLabel).textContent = months[month] + ' ' + year;
        },

        displayGlobal: (avgYears, totLen, avgLen) => {
    		document.querySelector(DOMstrings.labelAvgPark).textContent = avgYears;
    		document.querySelector(DOMstrings.labelTotalStreets).textContent = totLen;
    		document.querySelector(DOMstrings.labelAvgStreets).textContent = avgLen;
        },

        disableInputs: (type) =>{
        	let inputs, inputsArr;

        	if(type === 's'){
        		inputs = document.querySelectorAll(DOMstrings.parkInputs);
        		inputsArr = Array.from(inputs);

    			for(const cur of inputsArr) {
            		cur.disabled = true;
    			}
        	}else if(type === 'p'){
        		inputs = document.querySelectorAll(DOMstrings.streetInputs + ', ' + DOMstrings.addSizeStreet);
        		inputsArr = Array.from(inputs);

    			for(const cur of inputsArr) {
            		cur.disabled = true;
    			}
        	}
        },

        deleteItem: (selector) => {
        	let el = document.getElementById(selector);
            el.parentNode.removeChild(el);
        }
    }
})();

let controller = ((reportCtrl, UICtrl) => {
	let newItem;

	let setupEventListeners = function() {
		let inputsStreet, inputsStreetArr, inputsPark, inputsParkArr, typeInput;
        const DOM = UICtrl.getDOMstrings();
        
        inputsStreet = document.querySelectorAll(DOM.streetInputs + ', ' + DOM.addSizeStreet);
        inputsPark = document.querySelectorAll(DOM.parkInputs);

        inputsStreetArr = Array.from(inputsStreet);
        inputsParkArr = Array.from(inputsPark);
            
        for(const cur of inputsStreetArr) {
        	cur.onfocus = () => UICtrl.disableInputs(typeInput = 's');
        }

        for(const cur of inputsParkArr) {
            cur.onfocus = () => UICtrl.disableInputs(typeInput = 'p');
        }
        
        document.querySelector(DOM.saveStreetBtn).addEventListener('click', ctrlAddItem);
        document.querySelector(DOM.deleteBox).addEventListener('click', ctrlDeleteItem);
    };

    let updateGlobalData = () => {
    	
    	let [avgParkYears, totalLengthStreets, avgLengthStreets] = reportCtrl.calcGlobal();
    	
    	UICtrl.displayGlobal(avgParkYears, totalLengthStreets, avgLengthStreets);

    };

	let ctrlAddItem = () => {
        let input, detailType, itemID;
        
        detailType = '';
        itemID = reportCtrl.randomNumb();

        // 1. Get the field input data
        input = UICtrl.getInput();

        // 2. Add the item with ID to the report controller if some name is not empty
        if(input.nameStreet !== ''){
        	detailType = 's';
        	var [objItem, idItem] = reportCtrl.addItem(itemID, detailType, input.nameStreet, input.lengthStreet, input.sizeStreet, input.buildYearStreet);
        }else if(input.namePark !== ''){
        	detailType = 'p';
        	var [objItem, idItem] = reportCtrl.addItem(itemID, detailType, input.namePark, input.treeNumbPark, input.sizePark, input.buildYearPark);       	
        }

        // 3. Add the item to the UI
        UICtrl.addListItem(objItem, detailType, idItem);

       	// 4. Update global report data
        updateGlobalData();

        // 4. Clear all fields
        UICtrl.clearFields();
    };

    let ctrlDeleteItem = (event) => {
    	let itemId, splitID, type, id;
        
        itemId = event.target.parentNode.id
        splitID = itemId.split('-');
        type = splitID[0];
        id = parseInt(splitID[1]);
    	
    	// Delete item from data structure
    	reportCtrl.deleteItemStructure(id, type);


    	// Delete item from UI
    	UICtrl.deleteItem(itemId);

    	// Update global data
    	updateGlobalData();
    };

    return {
        init: () => {
        	UICtrl.displaySubtitle();
            setupEventListeners();
        }
    };

})(reportController, UIController);

controller.init();